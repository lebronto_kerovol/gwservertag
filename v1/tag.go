package v1

import (
	location "bitbucket.org/lebronto_kerovol/gwlocation/v1"
	"bitbucket.org/lebronto_kerovol/gwservertag/util"
)

type Tag struct {
	Version  string            `json:"version"`
	Location location.Location `json:"location"`
	Tasks    []string          `json:"tasks"`
	Code     string            `json:"code"`
}

func (tag *Tag) ContainTask(task string) bool {
	return util.Contain(tag.Tasks, task)
}
