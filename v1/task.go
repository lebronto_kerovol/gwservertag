package v1

//Base Tasks
const (
	TaskAuth         = "Authorization"
	TaskRegistration = "Registration"
	TaskRecovery     = "Recovery"
	TaskValidation   = "Validation"
	TaskJournal      = "Journal"
	TaskSecurity     = "Security"
	TaskSMTP         = "SMTP"
	TaskContent      = "Content"
)

//Administration Tasks
const (
	TaskAdministrationComplaint = "AdministrationComplaint"
	TaskAdministrationAccount   = "AdministrationAccount"
)

//Guest or Account Tasks
const (
	TaskUserChat = "UserChat"
)
