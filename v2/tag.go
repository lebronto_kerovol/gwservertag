package v2

import (
	location "bitbucket.org/lebronto_kerovol/gwlocation/v1"
	"bitbucket.org/lebronto_kerovol/gwservertag/util"
)

type Tag struct {
	Version   *string            `json:"version,omitempty"`
	Location  *location.Location `json:"location,omitempty"`
	Tasks     *[]string          `json:"tasks,omitempty"`
	Code      *string            `json:"code,omitempty"`
	CodeGroup *string            `json:"code-group,omitempty"`
}

func (tag *Tag) ContainTask(task string) bool {
	return util.Contain(*tag.Tasks, task)
}
