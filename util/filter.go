package util

func FilterString(arr *[]string, cond func(string) bool) []string {
	result := []string{}
	for _, element := range *arr {
		if cond(element) {
			result = append(result, element)
		}
	}
	return result
}
