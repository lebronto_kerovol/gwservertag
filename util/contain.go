package util

import (
	"sort"
)

func Contain(elements []string, pattern string) bool {
	i := sort.Search(len(elements), func(i int) bool { return pattern <= elements[i] })
	return (i < len(elements) && elements[i] == pattern)
}
